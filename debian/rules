#!/usr/bin/make -f
# This debian/rules file is provided as a template for normal perl
# packages. It was created by Marc Brockschmidt <marc@dch-faq.de> for
# the Debian Perl Group (http://pkg-perl.alioth.debian.org/) but may
# be used freely wherever it is useful.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# If set to a true value then MakeMaker's prompt function will
# always return the default without waiting for user input.
export PERL_MM_USE_DEFAULT=1

ifndef PERL
PERL = /usr/bin/perl
endif

TMP     =$(CURDIR)/debian/tmp
MTROOT  =$(TMP)/usr/share/movabletype
MTCGI   =$(TMP)/usr/lib/cgi-bin/movabletype
MTPERL  =$(TMP)/usr/share/perl5
MTWEBROOT =$(MTROOT)/static

build: build-arch build-indep
build-arch: build-stamp
build-indep: build-stamp
build-stamp:
	dh_testdir

	# Add commands to compile the package here

	touch $@

clean:
	dh_testdir
	dh_testroot

	debconf-updatepo

	dh_clean build-stamp install-stamp

install: install-stamp
install-stamp: build-stamp
	dh_testdir
	dh_testroot
	dh_prep

	install -d $(MTROOT) $(MTCGI) $(MTPERL) $(MTWEBROOT)
	install -d $(MTROOT)/tools
	install -d $(MTROOT)/themes
	install -d $(MTROOT)/plugins
	install -d $(MTROOT)/extlib

	cp --preserve=timestamps -r \
		default_templates \
		search_templates \
		tmpl \
		plugins \
		themes \
	$(MTROOT)
	cp --preserve=timestamps -r mt-static/* $(MTWEBROOT)
	
	install -m 755 tools/*             $(MTROOT)/tools
	install -m 755 *.cgi               $(MTCGI)
	
	cp --preserve=timestamps -r lib/*  $(MTPERL)

	# Hack to get things sorted for lenny; include these modules rather
	# than relying on the packages, due to local changes in MT package.
	# XXX should really fix this up post-lenny if we can persuade
	# Six Apart to release their changes to CPAN
	# However they have not so far been responsive:
	# http://www.sixapart.com/pipermail/mtos-dev/2008-May/001258.html
	# http://www.sixapart.com/pipermail/mtos-dev/2008-June/001452.html
	# 
	# March 2009: They match up now, but not holding my breath
	# in the long term so just commenting out.
	#cp --preserve=timestamps -r extlib/Data/ObjectDriver* $(MTROOT)/extlib/Data
	#cp --preserve=timestamps -r extlib/TheSchwartz* $(MTROOT)/extlib

	# Don't want to ship external modules
	rm -rf $(TMP)/usr/share/movabletype/plugins/*/lib/HTML
	rm -rf $(TMP)/usr/share/movabletype/plugins/*/lib/Text
	rmdir $(TMP)/usr/share/movabletype/plugins/Textile/lib/
	rm -rf $(TMP)/usr/share/movabletype/plugins/*/t

	# Tweak things for our layout
	mkdir -p $(TMP)/var/lib/movabletype-opensource/support
	# This directory contains dubious files in 4.33 release
	# (empty stats files)
	rm -rf $(MTWEBROOT)/support
	chown www-data $(TMP)/var/lib/movabletype-opensource/support

	mkdir -p $(TMP)/etc/movabletype-opensource
	ln -s /etc/movabletype-opensource/mt-config.cgi $(MTCGI)/mt-config.cgi
	ln -s /usr/share/movabletype/tmpl $(MTCGI)/tmpl
	ln -s /usr/share/movabletype/search_templates $(MTCGI)/search_templates
	ln -s /usr/share/movabletype/default_templates $(MTCGI)/default_templates
	ln -s /usr/share/movabletype/plugins $(MTCGI)/plugins

	# Web server config
	mkdir -p $(TMP)/etc/apache2/conf-available
	cp -p debian/movabletype-opensource.apache2 $(TMP)/etc/apache2/conf-available/movabletype-opensource.conf

	# Fix up some permissions
	find $(MTWEBROOT) $(MTROOT)/default_templates $(MTROOT)/search_templates \
		$(MTROOT)/tmpl $(MTROOT)/plugins $(MTPERL) -type f \! -name '*.cgi' -exec chmod 644 {} \;

	touch $@

binary-arch:
# We have nothing to do here for an architecture-independent package

binary-indep: build install
	dh_testdir
	dh_testroot
	dh_installdebconf
	dh_installchangelogs
	dh_installexamples
	dh_installdocs readme.html
	dh_install --sourcedir=debian/tmp
	dh_lintian
	dh_perl
	dh_link
	dh_compress
	dh_fixperms -X/var/lib/movabletype-opensource/support
	dh_installdeb
	dh_gencontrol
	dh_md5sums
	dh_builddeb

source diff:
	@echo >&2 'source and diff are obsolete - use dpkg-source -b'; false

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary

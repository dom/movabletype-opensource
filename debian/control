Source: movabletype-opensource
Section: web
Priority: optional
Build-Depends: debhelper (>= 7), po-debconf
Build-Depends-Indep: perl (>= 5.8.8-12)
Maintainer: Debian Movable Type and OpenMelody team <pkg-mt-om-devel@lists.alioth.debian.org>
Uploaders: Dominic Hargreaves <dom@earth.li>
Standards-Version: 3.9.4
Homepage: http://www.movabletype.org/opensource/
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-mt-om/movabletype-opensource.git
Vcs-Git: git://anonscm.debian.org/pkg-mt-om/movabletype-opensource.git

Package: movabletype-opensource
Architecture: all
Depends: ${misc:Depends},
 perl-modules,
 libclass-accessor-perl (>= 0.34),
 libimage-size-perl (>= 3.231),
 libjson-perl (>= 2.15),
 libwww-perl,
 libmime-encwords-perl (>= 1.011.1),
 libnet-openid-consumer-perl (>= 1.03),
 liburi-perl (>= 1.40),
 libxml-atom-perl (>= 0.38),
 libxml-sax-perl,
 libxml-xpath-perl,
 libyaml-tiny-perl (>= 1.40),
 libdbd-sqlite3-perl,
 libdbd-mysql-perl,
 libdbd-pg-perl,
 dbconfig-common,
 exim4 | mail-transport-agent,
 ucf,
 libhtml-parser-perl,
 liblucene-queryparser-perl,
 libclass-trigger-perl (>= 0.14),
 libdbi-perl,
 libclass-data-inheritable-perl (>= 0.08),
 libtheschwartz-perl (>= 1.07),
 libdata-objectdriver-perl (>= 0.08),
 libjs-jquery,
 perl-modules (>= 5.10.0) | libversion-perl,
 liburi-fetch-perl,
 libhtml-parser-perl,
 libipc-run-perl,
 libcrypt-ssleay-perl,
 libgd-gd2-perl,
 sqlite | mysql-client | postgresql-client,
 libalgorithm-diff-perl,
 libparams-validate-perl (>= 1.06),
 libcgi-pm-perl (>= 3.60),
 perl (>= 5.17.0) | libmath-bigint-perl (>= 1.997),
 libnet-oauth-perl (>= 0.28),
 libnet-smtp-tls-perl (>= 0.12),
 libauthen-sasl-perl,
 libnet-smtp-ssl-perl,
 libio-socket-ssl-perl,
 libnet-ssleay-perl,
 perlmagick
Pre-Depends: debconf
Recommends: libarchive-tar-perl,
 libsoap-lite-perl,
 libarchive-zip-perl
Provides: movabletype
Description: Well-known blogging engine
 MovableType is a popular blogging, or web publishing platform. It
 provides an easy to use web interface to publishing blogs and contains
 many features and plugins.
 .
 NOTE: Upstream have announced that 5.2.x will be the last open source
 release of Movable Type, It is likely that this package will be removed
 from Debian in 2015, and so it is not recommended for new installs.

Package: movabletype-plugin-core
Architecture: all
Depends: ${misc:Depends},
 libwww-perl,
 perl-modules,
 libxml-sax-perl,
 libhtml-parser-perl,
 libtext-textile-perl,
 libhtml-entities-numbered-perl,
 libjson-perl,
 libclass-accessor-perl
Description: Core Movable Type plugins
 These plugins are included with Movable Type Open Source:
 FormattedText, FormattedTextForTinyMCE, Markdown, MultiBlog, spamlookup,
 StyleCatcher, TinyMCE, Textile, WXRImporter

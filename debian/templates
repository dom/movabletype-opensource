Template: movabletype-opensource/admin_account_warn
Type: boolean
_Description: Install Movable Type?
 When configuring a new database with this package (for example when
 installing it for the first time) the Movabletype install starts off
 being non-password-protected; that is, the first person to visit
 http://your.server/cgi-bin/movabletype/mt.cgi will be able to set the
 admin password and take control of your Movable Type installation.
 .
 You should take appropriate measures, such as remembering to configure
 the admin account straight after the install is completed, or restricting
 access to your web server.

Template: movabletype-opensource/umask_warn
Type: note
_Description: Insecure umask setting
 Due to an error preparing a previous version of the Movable Type package,
 a typo was introduced into the default configuration file which caused
 a dangerous umask to be set when publishing. This may have caused blog
 files to be created world-writable.
 .
 You should check and fix the permissions of such files, and ensure that
 the typo fix (HTMLUask should be HTMLUmask) is applied to your
 configuration file, /etc/opensource-movabletype/mt-config.cgi, once this
 package installation has completed.

Template: movabletype-opensource/schema_upgrade
Type: boolean
_Description: Continue with package upgrade which may need schema upgrades?
 You are about to upgrade the Movable Type package to a version which may
 include a new database schema version. To ensure continued functionality
 of Movable Type sites, you should log into any configured instances with
 an administrator account immediately after this package upgrade has
 completed, where you will be prompted to upgrade databases as required.

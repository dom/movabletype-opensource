#!/usr/bin/perl -w

# Movable Type (r) Open Source (C) 2001-2013 Six Apart, Ltd.
# This program is distributed under the terms of the
# GNU General Public License, version 2.
#
# $Id$

use strict;
use lib '/usr/share/movabletype/extlib';
use MT::Bootstrap App => 'MT::App::Search';
